package aquacore.launchserver.resource;

import java.net.URL;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class TextureInfo {
	private URL url;
}
