package aquacore.launchserver.resource;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TexturesResource {
	private long timestamp = System.currentTimeMillis();
	private String profileId;
	private String profileName;
	private Map<String, TextureInfo> textures = new HashMap<>(); 
}
