package aquacore.launchserver;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

import aquacore.launchserver.crypto.CryptoStorageBean;

@SpringBootApplication
@EnableAutoConfiguration
@Import(ClientResourceConfiguration.class)
public class Application {
	private CryptoStorageBean cryptoStorageInstance = null;

	@Bean
	public CryptoStorageBean cryptoStorage() {
		if (cryptoStorageInstance == null)
			cryptoStorageInstance = new CryptoStorageBean();
		return cryptoStorageInstance;
	}

	@Bean
	public AbstractRequestLoggingFilter requestLoggingFilter() {
		AbstractRequestLoggingFilter loggingFilter = new AbstractRequestLoggingFilter() {

			@Override
			protected void beforeRequest(HttpServletRequest request, String message) {
				logger.info(message);
			}

			@Override
			protected void afterRequest(HttpServletRequest request, String message) {
			}
			
		};
		loggingFilter.setIncludeClientInfo(true);
		loggingFilter.setIncludeQueryString(true);
		loggingFilter.setIncludePayload(true);
		return loggingFilter;
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
