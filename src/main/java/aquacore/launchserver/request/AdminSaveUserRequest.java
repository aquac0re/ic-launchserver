package aquacore.launchserver.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminSaveUserRequest {
	private String username;
	private int accessLevel;
	private String newpass;
	private String newpass2;
}
