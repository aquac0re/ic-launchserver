package aquacore.launchserver.db;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
	Optional<User> findByUuid(UUID uuid);
	Page<User> findByAccessLevelLessThanEqual(int level, Pageable pageable);
	int countByAccessLevelLessThanEqual(int level);
	Page<User> findAll(Pageable pageable);
}
