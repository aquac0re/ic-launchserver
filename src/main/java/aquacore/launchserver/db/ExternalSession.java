package aquacore.launchserver.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name="aq_esessions")
public class ExternalSession {
	@Id
	@GeneratedValue
	@Column
	private int id;
	
	@Column
	private String username;
	
	@Column
	private String accessToken;
}
