package aquacore.launchserver.db;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name="aq_sessions")
public class Session {
	@Id
	@Column(columnDefinition = "BINARY(16)", unique = true)
	private UUID uuid;
	
	@Column(unique = true)
	private String username;
	
	@Column
	private String accessToken;
	
	@Column
	private String serverId;
}
