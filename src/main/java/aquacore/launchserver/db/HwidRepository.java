package aquacore.launchserver.db;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HwidRepository extends CrudRepository<HWIDEntry, Integer> {
	Optional<HWIDEntry> findByUsernameAndNameAndValueAndSuccess(String username, String name, String value, boolean success);
	Optional<HWIDEntry> findByUsernameAndNameAndValueAndBanned(String username, String name, String value, boolean banned);
}
