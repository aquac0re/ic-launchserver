package aquacore.launchserver.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "aq_hwid", uniqueConstraints = { @UniqueConstraint(columnNames = { "name", "value", "username", "success" }) })
public class HWIDEntry {
	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String username;
	
	@Column
	private String name;
	
	@Column
	private String value;
	
	@Column
	private boolean success;
	
	@Column(nullable = false)
	@ColumnDefault("false")
	private boolean banned;
}
