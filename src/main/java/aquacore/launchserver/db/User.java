package aquacore.launchserver.db;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "aq_users")
public class User {
	@Id
	@Column(unique = true)
	private String username;

	@Column(columnDefinition = "BINARY(16)", unique = true)
	private UUID uuid;

	@Column
	private String passwordHash;

	@Column(nullable = false)
	@ColumnDefault("0")
	private int accessLevel;
}
