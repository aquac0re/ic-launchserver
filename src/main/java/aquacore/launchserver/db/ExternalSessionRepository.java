package aquacore.launchserver.db;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExternalSessionRepository extends CrudRepository<ExternalSession, UUID> {
	Optional<ExternalSession> findByAccessToken(String accessToken);
}
