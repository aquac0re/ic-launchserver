package aquacore.launchserver.controller;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.KeyAgreement;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHPublicKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import aquacore.launchserver.crypto.CryptoStorageBean;
import aquacore.launchserver.crypto.CryptoStorageBean.CryptoStorageElement;
import aquacore.launchserver.crypto.DiffieHellman;
import aquacore.launchserver.response.CryptoInitResponse;

@Controller
public class CryptoController {
	@Autowired
	public CryptoStorageBean cryptoStorage;

	@GetMapping(path = "/cryptokeys", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody CryptoInitResponse cryptokeys(@RequestParam(defaultValue = "") String iv,
			@RequestParam(defaultValue = "") String uid) throws NoSuchAlgorithmException, DecoderException {
		if (uid.isEmpty() || iv.isEmpty()) {
			return null;
		}
		CryptoInitResponse cir = DiffieHellman.init();
		byte[] ivBytes = Hex.decodeHex(iv);
		cryptoStorage.put(uid, ivBytes, cir.getKp());
		return cir;
	}

	@GetMapping(path = "/crypto2", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String crypto2(@RequestParam(defaultValue = "") String uid,
			@RequestParam(defaultValue = "") String B)
			throws NoSuchAlgorithmException, DecoderException, InvalidKeyException, InvalidKeySpecException {
		if (uid.isEmpty() || B.isEmpty() || !cryptoStorage.containsKey(uid)) {
			return "";
		}
		CryptoStorageElement cse = cryptoStorage.get(uid);
		KeyAgreement ka = KeyAgreement.getInstance("DH");
		ka.init(cse.getKp().getPrivate());
		KeyFactory kf = KeyFactory.getInstance("DH");
		Key key = kf.generatePublic(
				new DHPublicKeySpec(new BigInteger(B, 16), ((DHPublicKey) cse.getKp().getPublic()).getParams().getP(),
						((DHPublicKey) cse.getKp().getPublic()).getParams().getG()));
		ka.doPhase(key, true);
		MessageDigest digest = MessageDigest.getInstance("MD5");
		digest.reset();
		digest.update(ka.generateSecret());
		cse.setSecret(digest.digest()); // Done.
		return "OK";
	}
}
