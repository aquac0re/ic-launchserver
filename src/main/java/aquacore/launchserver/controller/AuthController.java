package aquacore.launchserver.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.AbstractMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import aquacore.launchserver.crypto.CryptoStorageBean;
import aquacore.launchserver.crypto.CryptoStorageBean.CryptoStorageElement;
import aquacore.launchserver.db.HwidRepository;
import aquacore.launchserver.db.Session;
import aquacore.launchserver.db.SessionRepository;
import aquacore.launchserver.db.User;
import aquacore.launchserver.db.UserRepository;
import aquacore.launchserver.request.LoginRequest;
import aquacore.launchserver.request.NewJoinRequest;
import aquacore.launchserver.request.RegisterRequest;
import aquacore.launchserver.response.ErrorResponse;
import aquacore.launchserver.response.NewJoinResponse;
import aquacore.launchserver.response.ProfileResponse;
import aquacore.launchserver.response.SessionResponse;
import aquacore.launchserver.util.HWIDUtils;
import aquacore.launchserver.util.ProfileUtils;
import aquacore.launchserver.util.UUIDType5;

@Controller
public class AuthController {
	@Autowired
	public UserRepository userRepository;

	@Autowired
	public SessionRepository sessionRepository;

	@Autowired
	public HwidRepository hwidRepository;

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	@Autowired
	public CryptoStorageBean cryptoStorage;

	private static final SecureRandom secureRandom = new SecureRandom();
	private static final Pattern usernamePattern = Pattern.compile("^[A-Za-z0-9_-]{4,16}$");
	private static final UUID NS_PLAYER = UUID.fromString("3a273386-e015-4a08-970d-e8cbc93223fc");

	@GetMapping(path = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody SessionResponse doLogin(@RequestParam(defaultValue = "") String username,
			@RequestParam(defaultValue = "") String password, @RequestParam(defaultValue = "") String hwid,
			HttpServletRequest request) {
		SessionResponse response = new SessionResponse();
		if (username.isEmpty() || password.isEmpty()) {
			response.setError("Поле пароля или логина пусты.");
			HWIDUtils.sendHWID(hwidRepository, username, request.getRemoteAddr(), hwid, false);
			return response;
		}
		Optional<User> user = userRepository.findById(username);
		if (user.isPresent()) {
			if (user.get().getPasswordHash().equals(DigestUtils.sha256Hex(password))) {
				if (user.get().getAccessLevel() <= AdminPanelController.ACCESS_BANNED
						|| !HWIDUtils.checkHWID(hwidRepository, username, request.getRemoteAddr(), hwid)) {
					response.setError("Вы забанены.");
					HWIDUtils.sendHWID(hwidRepository, username, request.getRemoteAddr(), hwid, false);
					return response;
				}
				Session session = new Session();
				session.setUuid(user.get().getUuid());
				session.setUsername(username);
				byte[] token = new byte[16];
				secureRandom.nextBytes(token);
				session.setAccessToken(Hex.encodeHexString(token));
				sessionRepository.save(session);

				response.setAccessToken(session.getAccessToken());
				response.setUuid(session.getUuid().toString());
				HWIDUtils.sendHWID(hwidRepository, username, request.getRemoteAddr(), hwid, true);
				return response;
			} else {
				response.setError("Неверный пароль.");
				HWIDUtils.sendHWID(hwidRepository, username, request.getRemoteAddr(), hwid, false);
				return response;
			}
		} else {
			response.setError("Пользователь не существует.");
			HWIDUtils.sendHWID(hwidRepository, username, request.getRemoteAddr(), hwid, false);
			return response;
		}
	}

	@PostMapping(path = "/register", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ErrorResponse doRegister(@RequestBody RegisterRequest request) {
		if (request.getUsername() == null || request.getPassword() == null || request.getUsername().isEmpty()
				|| request.getPassword().isEmpty()) {
			return new ErrorResponse("Имя пользователя/пароль не должны быть пустыми");
		} else if (userRepository.existsById(request.getUsername())) {
			return new ErrorResponse("Пользователь уже существует");
		} else if (request.getPassword().length() < 6) {
			return new ErrorResponse("Пароль должен быть минимум 6 символов");
		} else if (!usernamePattern.matcher(request.getUsername()).matches()) {
			return new ErrorResponse(
					"Ник может содержать только цифры, подчеркивание, дефис и латинские буквы, а также должен быть от 4 до 16 символов!");
		} else {
			User user = new User();
			user.setUsername(request.getUsername());
			user.setUuid(UUIDType5.nameUUIDFromNamespaceAndString(NS_PLAYER, request.getUsername()));
			user.setPasswordHash(DigestUtils.sha256Hex(request.getPassword()));
			userRepository.save(user);
			return new ErrorResponse(null);
		}
	}

	@GetMapping(path = "/legacyjoin", produces = MediaType.TEXT_PLAIN_VALUE)
	public @ResponseBody String doLegacyJoin(@RequestParam(defaultValue = "") String user,
			@RequestParam(defaultValue = "") String sessionId, @RequestParam(defaultValue = "") String serverId) {
		if (user.isEmpty()) {
			return "Invalid username";
		} else if (serverId.isEmpty()) {
			return "Invalid server";
		} else if (sessionId.isEmpty()) {
			return "Invalid session";
		} else {
			Optional<User> profile = userRepository.findById(user);
			if (!profile.isPresent()) {
				return "Invalid username";
			} else {
				Optional<Session> session = sessionRepository.findById(profile.get().getUuid());
				if (session.isPresent() && session.get().getAccessToken().equals(sessionId)) {
					session.get().setServerId(serverId);
					sessionRepository.save(session.get());
					return "OK";
				} else {
					return "Invalid session";
				}
			}
		}
	}

	@GetMapping(path = "/legacycheck", produces = MediaType.TEXT_PLAIN_VALUE)
	public @ResponseBody String doLegacyCheck(@RequestParam(defaultValue = "") String user,
			@RequestParam(defaultValue = "") String serverId) {
		if (user.isEmpty()) {
			return "Invalid username";
		} else if (serverId.isEmpty()) {
			return "Invalid server";
		} else {
			Optional<User> profile = userRepository.findById(user);
			if (!profile.isPresent()) {
				return "Invalid username";
			} else {
				Optional<Session> session = sessionRepository.findById(profile.get().getUuid());
				if (session.isPresent() && session.get().getServerId().equals(serverId)) {
					return "YES";
				} else {
					return "Invalid session";
				}
			}
		}
	}

	@PostMapping(path = "/join", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody NewJoinResponse doNewJoin(@RequestBody NewJoinRequest request) {
		if (request.getSelectedProfile() == null || request.getAccessToken() == null || request.getServerId() == null) {
			NewJoinResponse response = new NewJoinResponse();
			response.setError("Bad request");
			response.setErrorMessage("Bad request");
			return response;
		} else {
			UUID uuid = UUID.fromString(request.getSelectedProfile()
					.replaceFirst("(\\p{XDigit}{8})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}+)", "$1-$2-$3-$4-$5"));
			Optional<Session> session = sessionRepository.findById(uuid);
			if (session.isPresent()) {
				if (session.get().getAccessToken().equals(request.getAccessToken())) {
					session.get().setServerId(request.getServerId());
					sessionRepository.save(session.get());
					NewJoinResponse response = new NewJoinResponse();
					response.setId(request.getSelectedProfile());
					response.setName(session.get().getUsername());
					return response;
				} else {
					NewJoinResponse response = new NewJoinResponse();
					response.setError("Bad session");
					response.setErrorMessage("Bad session");
					return response;
				}
			} else {
				NewJoinResponse response = new NewJoinResponse();
				response.setError("Bad session");
				response.setErrorMessage("Bad session");
				return response;
			}
		}
	}

	@GetMapping(path = "/hasJoined", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ProfileResponse doNewHasJoined(@RequestParam(defaultValue = "") String username,
			@RequestParam(defaultValue = "") String serverId, @RequestParam(defaultValue = "") String ip)
			throws MalformedURLException, JsonProcessingException {
		Optional<User> user = userRepository.findById(username);
		if (user.isPresent()) {
			Optional<Session> session = sessionRepository.findById(user.get().getUuid());
			if (session.isPresent()) {
				if (session.get().getServerId().equals(serverId)) {
					return ProfileUtils.getProfileFromUser(jacksonObjectMapper, user);
				} else {
					ProfileResponse response = new ProfileResponse();
					response.setError("Bad session");
					response.setErrorMessage("Bad session");
					return response;
				}

			} else {
				ProfileResponse response = new ProfileResponse();
				response.setError("Bad session");
				response.setErrorMessage("Bad session");
				return response;
			}
		} else {
			ProfileResponse response = new ProfileResponse();
			response.setError("Bad username");
			response.setErrorMessage("Bad username");
			return response;
		}
	}

	@GetMapping(path = "/profile/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ProfileResponse doProfile(@PathVariable("uuid") String uuidString)
			throws MalformedURLException, JsonProcessingException {
		UUID uuid = UUID.fromString(uuidString
				.replaceFirst("(\\p{XDigit}{8})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}+)", "$1-$2-$3-$4-$5"));
		Optional<User> user = userRepository.findByUuid(uuid);
		return ProfileUtils.getProfileFromUser(jacksonObjectMapper, user);
	}

	@GetMapping(path = "/profile2/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ProfileResponse doProfile2(@PathVariable String username)
			throws MalformedURLException, JsonProcessingException {
		Optional<User> user = userRepository.findById(username);
		return ProfileUtils.getProfileFromUser(jacksonObjectMapper, user);
	}

	@PostMapping(path = "/crypto/login", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public @ResponseBody byte[] doCryptoLogin(@RequestParam(defaultValue = "") String uid, @RequestBody byte[] request,
			HttpServletRequest httprequest) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException,
			JsonParseException, JsonMappingException, IOException {
		if (uid.isEmpty() || !cryptoStorage.containsKey(uid)) {
			return new byte[0];
		}
		CryptoStorageElement cse = cryptoStorage.get(uid);
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(cse.getSecret(), "AES"), new IvParameterSpec(cse.getIv()));
		String decrypted = new String(cipher.doFinal(request), "UTF-8");
		LoginRequest lr = jacksonObjectMapper.readValue(decrypted, LoginRequest.class);
		SessionResponse sr = doLogin(lr.getUsername(), lr.getPassword(), lr.getHwid(), httprequest);
		byte[] respBytes = jacksonObjectMapper.writeValueAsBytes(sr);
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(cse.getSecret(), "AES"), new IvParameterSpec(cse.getIv()));
		return cipher.doFinal(respBytes);
	}

	@GetMapping(path = "/skins/{username}", produces = MediaType.IMAGE_PNG_VALUE)
	public @ResponseBody ResponseEntity<InputStreamResource> get_File(@PathVariable String username)
			throws IOException {
		username = username.substring(0, username.length() - 4);

		File file = new File("./client/Skins/default.png");
		if (userRepository.existsById(username)) {
			file = new File("./client/Skins/" + userRepository.findById(username).get().getUuid() + ".png");
			if (!file.exists()) {
				file = new File("./client/Skins/default.png");
			}
		}
		InputStreamResource isr = new InputStreamResource(new FileInputStream(file));
		return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).contentLength(file.length()).body(isr);
	}

	private static boolean isPowerOfTwo(int number) {
		return number > 0 && ((number & (number - 1)) == 0);
	}

	@PostMapping(path = "/skinul", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ErrorResponse doUploadSkin(@RequestParam(value = "skin", required = false) MultipartFile skin,
			@RequestParam("token") String token, @RequestParam("uuid") String uuid) throws IOException {
		if (!sessionRepository.existsById(UUID.fromString(uuid))
				|| !sessionRepository.findById(UUID.fromString(uuid)).get().getAccessToken().equals(token)) {
			return new ErrorResponse("Invalid token");
		}
		if (skin == null) {
			new File("./client/Skins/" + uuid + ".png").delete();
			return new ErrorResponse();
		}
		InputStream b4is = skin.getInputStream();
		ImageInputStream iis = ImageIO.createImageInputStream(b4is);
		Iterator<ImageReader> irs = ImageIO.getImageReaders(iis);
		if (irs.hasNext()) {
			ImageReader ir = irs.next();
			ir.setInput(iis);
			int w = ir.getWidth(0);
			int h = ir.getHeight(0);
			if (w > 128 || h > 64 || h * 2 != w || !isPowerOfTwo(h)) {
				return new ErrorResponse("Макс. размер - 128x64.");
			} else {
				b4is.close();

				b4is = skin.getInputStream();

				FileUtils.copyInputStreamToFile(b4is, new File("./client/Skins/" + uuid + ".png"));

				b4is.close();

			}
		} else {
			return new ErrorResponse("Файл не в формате png.");
		}

		return new ErrorResponse();
	}

	@RequestMapping(path = "/clients", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object getClients() throws IOException, JsonProcessingException {
		List<String> clientNames = jacksonObjectMapper
				.readValue(new File("client/newclients.json"), jacksonObjectMapper.getTypeFactory()
						.constructCollectionType(List.class, String.class));

		return clientNames.stream().map(name -> {
			try {
				return new AbstractMap.SimpleImmutableEntry<String, JsonNode>(name, jacksonObjectMapper.readTree(new File("client/" + name + "/policy.json")));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
}
