package aquacore.launchserver.controller;

import java.security.SecureRandom;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import aquacore.launchserver.db.ExternalSession;
import aquacore.launchserver.db.ExternalSessionRepository;
import aquacore.launchserver.db.User;
import aquacore.launchserver.db.UserRepository;
import aquacore.launchserver.request.AdminSaveUserRequest;
import aquacore.launchserver.request.LoginRequest;

@Controller
public class AdminPanelController {
	@Autowired
	public UserRepository userRepository;

	@Autowired
	public ExternalSessionRepository externalSessionRepository;

	private static final SecureRandom secureRandom = new SecureRandom();
	public static final int ACCESS_MINIMUM = 100;
	public static final int ACCESS_INVALID = -1;
	public static final int ACCESS_BANNED = -2;

	private int getAccessLevelForToken(String token) {
		Optional<ExternalSession> session = externalSessionRepository.findByAccessToken(token);
		if (!session.isPresent()) {
			return ACCESS_INVALID;
		}
		Optional<User> user = userRepository.findById(session.get().getUsername());
		if (!user.isPresent()) {
			return ACCESS_INVALID;
		}
		return user.get().getAccessLevel();
	}

	private void resetAuth(HttpServletResponse response) {
		Cookie cookie = new Cookie("token", "");
		cookie.setMaxAge(0);
		response.addCookie(cookie);
	}

	private boolean checkAccessLevel(HttpServletResponse response, String token) {
		if (token != null) {
			int accessLevel = getAccessLevelForToken(token);
			if (accessLevel < ACCESS_MINIMUM) {
				resetAuth(response);
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	@PostMapping(path = "/admin/do_auth.jsp", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public RedirectView adminDoAuth(LoginRequest req, HttpServletResponse response) {
		Optional<User> user = userRepository.findById(req.getUsername());
		if (user.isPresent()) {

			if (user.get().getPasswordHash().equalsIgnoreCase(DigestUtils.sha256Hex(req.getPassword()))) {
				if (user.get().getAccessLevel() < ACCESS_MINIMUM) {
					System.out.println("Adminpanel: Invalid access level");
					return new RedirectView("/admin/?error=forbidden");
				} else {
					ExternalSession session = new ExternalSession();
					session.setUsername(req.getUsername());
					byte[] token = new byte[16];
					secureRandom.nextBytes(token);
					session.setAccessToken(Hex.encodeHexString(token));
					externalSessionRepository.save(session);
					Cookie cookie = new Cookie("token", session.getAccessToken());
					cookie.setMaxAge(60 * 60 * 24 * 365 * 10);
					cookie.setPath("/admin");
					response.addCookie(cookie);
					return new RedirectView("/admin/");
				}
			} else {
				System.out.println("Adminpanel: Invalid password");
				return new RedirectView("/admin/?error=invalid");
			}
		} else {
			System.out.println("Adminpanel: Invalid username");
			return new RedirectView("/admin/?error=invalid");
		}
	}

	@RequestMapping("/admin/logout")
	public RedirectView adminLogout(HttpServletResponse response) {
		resetAuth(response);
		return new RedirectView("/admin/");
	}

	@RequestMapping("/admin/users")
	public Object adminUsers(HttpServletResponse response, Model model,
			@CookieValue(name = "token", required = false) String token,
			@RequestParam(name = "page", defaultValue = "0") int page) {
		if (checkAccessLevel(response, token)) {
			model.addAttribute("userRepository", userRepository);
			model.addAttribute("template", "Users");
			PageRequest pageRequest = PageRequest.of(page, 50, Sort.by(Order.asc("username")));
			model.addAttribute("pageRequest", pageRequest);
			model.addAttribute("numPages", userRepository.count() / 50);
			model.addAttribute("maxAccessLevel", 200000);
			model.addAttribute("headerTitle", "Пользователи");
			return "admin";
		} else {
			return new RedirectView("/admin/");
		}
	}

	@RequestMapping("/admin/users/{username}")
	public Object adminUser(HttpServletResponse response, Model model,
			@CookieValue(name = "token", required = false) String token, @PathVariable("username") String username) {
		if (checkAccessLevel(response, token)) {
			model.addAttribute("template", "User");
			model.addAttribute("user", userRepository.findById(username).get());
			return "admin";
		} else {
			return new RedirectView("/admin/");
		}
	}

	@RequestMapping("/admin/banlist")
	public Object adminBanList(HttpServletResponse response, Model model,
			@CookieValue(name = "token", required = false) String token,
			@RequestParam(name = "page", defaultValue = "0") int page) {
		if (checkAccessLevel(response, token)) {
			model.addAttribute("userRepository", userRepository);
			model.addAttribute("template", "Users");
			PageRequest pageRequest = PageRequest.of(page, 50, Sort.by(Order.asc("username")));
			model.addAttribute("pageRequest", pageRequest);
			model.addAttribute("maxAccessLevel", ACCESS_BANNED);
			model.addAttribute("headerTitle", "Банлист");
			model.addAttribute("numPages", userRepository.countByAccessLevelLessThanEqual(ACCESS_BANNED) / 50);
			return "admin";
		} else {
			return new RedirectView("/admin/");
		}
	}

	@PostMapping(path = "/admin/users/{username}/save")
	public Object adminUserSave(HttpServletResponse response, Model model,
			@CookieValue(name = "token", required = false) String token, @PathVariable("username") String username,
			AdminSaveUserRequest body) {
		if (checkAccessLevel(response, token)) {
			Optional<User> _user = userRepository.findById(username);
			if (!_user.isPresent()) {
				return new RedirectView("/admin/users/" + username);
			}
			User user = _user.get();
			if (!body.getUsername().equals(username)) {
				userRepository.deleteById(username);
				user.setUsername(body.getUsername());
			}
			user.setAccessLevel(body.getAccessLevel());
			if (body.getNewpass() != null && body.getNewpass2() != null && !body.getNewpass().isEmpty()
					&& body.getNewpass().equals(body.getNewpass2())) {
				user.setPasswordHash(DigestUtils.sha256Hex(body.getNewpass()));
			}
			userRepository.save(user);
			return new RedirectView("/admin/users/" + body.getUsername());
		} else {
			return new RedirectView("/admin/");
		}
	}

	@RequestMapping("/admin")
	public Object adminRoot(HttpServletResponse response, Model model,
			@RequestParam(defaultValue = "none") String error,
			@CookieValue(name = "token", required = false) String token) {
		if (checkAccessLevel(response, token)) {
			return new RedirectView("/admin/users/");
		} else {
			model.addAttribute("error", error);
			return "adminRoot";
		}
	}

}
