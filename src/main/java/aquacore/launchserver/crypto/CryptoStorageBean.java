package aquacore.launchserver.crypto;

import java.security.KeyPair;
import java.util.HashMap;
import java.util.Map;

import aquacore.launchserver.crypto.CryptoStorageBean.CryptoStorageElement;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Delegate;

public class CryptoStorageBean implements Map<String, CryptoStorageElement> {
	@Getter
	@Setter
	@RequiredArgsConstructor
	public class CryptoStorageElement {
		private final byte[] iv;
		private final KeyPair kp;
		private byte[] secret = null;
	}

	@Delegate
	private Map<String, CryptoStorageElement> elements = new HashMap<>();
	
	public void put(String uid, byte[] iv, KeyPair kp) {
		elements.put(uid, new CryptoStorageElement(iv, kp));
	}
}
