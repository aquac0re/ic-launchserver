package aquacore.launchserver.crypto;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;

import aquacore.launchserver.response.CryptoInitResponse;

public class DiffieHellman {
	public static CryptoInitResponse init() throws NoSuchAlgorithmException {
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("DH");
		kpg.initialize(1024);
		KeyPair kp = kpg.generateKeyPair();
		DHParameterSpec dhSpec = ((DHPublicKey) kp.getPublic()).getParams();
		return new CryptoInitResponse(dhSpec.getP(), dhSpec.getG(), ((DHPublicKey) kp.getPublic()).getY(), dhSpec.getL(), kp);
	}
}
