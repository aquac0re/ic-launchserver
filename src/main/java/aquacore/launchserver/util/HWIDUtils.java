package aquacore.launchserver.util;

import aquacore.launchserver.db.HWIDEntry;
import aquacore.launchserver.db.HwidRepository;

public class HWIDUtils {

	public static void sendHWID(HwidRepository hwidRepository, String username, String ip, String hwid,
			boolean isSuccess) {
		do {
			if (hwidRepository.findByUsernameAndNameAndValueAndSuccess(username, "ip", ip, isSuccess).isPresent())
				break;
			HWIDEntry iphwe = new HWIDEntry();
			iphwe.setUsername(username);
			iphwe.setSuccess(isSuccess);
			iphwe.setName("ip");
			iphwe.setValue(ip);
			hwidRepository.save(iphwe);
		} while (false);
		if (hwid == null || hwid.isEmpty())
			return;
		String[] spl = hwid.split("@@SEP@@");
		for (String s : spl) {
			String[] spl2 = s.split("@@VAL@@");
			if (spl2.length != 2)
				continue;
			if (hwidRepository
					.findByUsernameAndNameAndValueAndSuccess(username, spl2[0], spl2[1], isSuccess)
					.isPresent())
				continue;
			HWIDEntry hwe = new HWIDEntry();
			hwe.setUsername(username);
			hwe.setSuccess(isSuccess);
			hwe.setName(spl2[0]);
			hwe.setValue(spl2[1]);
			hwidRepository.save(hwe);
		}
	}

	public static boolean checkHWID(HwidRepository hwidRepository, String username, String ip, String hwid) {
		if (hwidRepository.findByUsernameAndNameAndValueAndBanned(username, "ip", ip, true).isPresent())
			return false;
		if (hwid == null || hwid.isEmpty())
			return true;
		String[] spl = hwid.split("@@SEP@@");
		for (String s : spl) {
			String[] spl2 = s.split("@@VAL@@");
			if (spl2.length != 2)
				continue;
			if (hwidRepository.findByUsernameAndNameAndValueAndBanned(username, spl2[0], spl2[1], true).isPresent())
				return false;
		}
		return true;
	}
}
