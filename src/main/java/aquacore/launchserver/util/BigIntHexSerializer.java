package aquacore.launchserver.util;

import java.io.IOException;
import java.math.BigInteger;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class BigIntHexSerializer extends StdSerializer<BigInteger> {
	private static final long serialVersionUID = 1L;

	public BigIntHexSerializer(Class<?> t, boolean dummy) {
		super(t, dummy);
	}

	public BigIntHexSerializer() {
		super((Class<BigInteger>) null);
	}

	public BigIntHexSerializer(Class<BigInteger> t) {
		super(t);
	}

	public BigIntHexSerializer(JavaType type) {
		super(type);
	}

	public BigIntHexSerializer(StdSerializer<?> src) {
		super(src);
	}

	@Override
	public void serialize(BigInteger value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		String s = value.toString(16);
		if (s.length() % 2 != 0)
			s = "0" + s;
		gen.writeString(s);
	}

}
