package aquacore.launchserver.util;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import org.apache.commons.codec.binary.Base64;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import aquacore.launchserver.db.User;
import aquacore.launchserver.resource.TextureInfo;
import aquacore.launchserver.resource.TexturesResource;
import aquacore.launchserver.response.AccountProperty;
import aquacore.launchserver.response.ProfileResponse;

public class ProfileUtils {
	public static ProfileResponse getProfileFromUser(ObjectMapper jacksonObjectMapper, Optional<User> user)
			throws MalformedURLException, JsonProcessingException {
		if (user.isPresent()) {
			String skinURL = "";
			if (new File("client/Skins/" + user.get().getUuid().toString() + ".png").exists()) {
				skinURL = "http://localhost:8080/" + "client/Skins/" + user.get().getUuid().toString() + ".png";
			} else {
				skinURL = "http://localhost:8080/" + "client/Skins/default.png";
			}

			TextureInfo skinInfo = new TextureInfo(new URL(skinURL));

			TexturesResource tex = new TexturesResource();
			tex.getTextures().put("SKIN", skinInfo);
			tex.setProfileName(user.get().getUsername());
			tex.setProfileId(user.get().getUuid().toString().replaceAll("-", ""));

			AccountProperty texProperty = new AccountProperty();
			texProperty.setName("textures");
			texProperty.setSignature("Cg==");
			texProperty.setValue(Base64.encodeBase64String(jacksonObjectMapper.writeValueAsBytes(tex)));

			ProfileResponse response = new ProfileResponse();
			response.setId(user.get().getUuid().toString().replaceAll("-", ""));
			response.setName(user.get().getUsername());
			response.getProperties().add(texProperty);

			return response;
		} else {
			ProfileResponse response = new ProfileResponse();
			response.setError("Bad UUID");
			response.setErrorMessage("Bad UUID");
			return response;
		}
	}
}
