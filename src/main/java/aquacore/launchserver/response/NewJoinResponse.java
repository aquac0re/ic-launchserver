package aquacore.launchserver.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class NewJoinResponse {
	private String error;
	private String errorMessage;
	private String id;
	private String name;
}
