package aquacore.launchserver.response;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ProfileResponse {
	private String error;
	private String errorMessage;
	private String id;
	private String name;
	private final List<AccountProperty> properties = new ArrayList<>();
}
