package aquacore.launchserver.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class AccountProperty {
	private String name;
	private String value;
	private String signature;
}
