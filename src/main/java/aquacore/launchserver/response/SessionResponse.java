package aquacore.launchserver.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SessionResponse {
	private String error = null;
	private String accessToken;
	private String uuid;
}
