package aquacore.launchserver.response;

import java.math.BigInteger;
import java.security.KeyPair;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import aquacore.launchserver.util.BigIntHexSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CryptoInitResponse {
	@JsonSerialize(using = BigIntHexSerializer.class)
	private BigInteger p, g;
	@JsonSerialize(using = BigIntHexSerializer.class)
    @JsonProperty("A")
	private BigInteger A;
	private int l;
    @JsonIgnore
	private KeyPair kp;
}
